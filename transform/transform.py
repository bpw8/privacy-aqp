import pandas as pd
import csv

from sqlalchemy import create_engine
engine = create_engine('postgresql://@localhost:5432/census')

with open('parsed_queries.csv', 'r') as queries_in:
    """ Opens the parsed_queries file and reads in column 4 which contains
    the query, and executes it. 

    Upon execution, our postgres engine will parse it, extract the predicate, 
    and log the results to a file. The file is then loaded in through parse_log.py
    which actually converts the predicate to CNF/DNF form. 

    In other words, this program doesn't produce any visible output: postgres
    generates output per query to a log file.
    """
    query_reader = csv.reader(queries_in)
    conn = engine.connect()
    for row in query_reader:
        #column 4 is the query
        query = row[3]
        query = query.lower()
        query = query.replace("\"", "")
       
        if not query or query == "none":
            continue
        else:
            print(query)
            try:
                conn.execute(query)
            except:
                print("Exception in query processing: {0}".format(query))
