import csv
import string
import sympy
import json
from sympy.logic.boolalg import to_cnf, And, Or
from sympy.logic import simplify_logic
from sympy import pprint
letters = string.ascii_uppercase
expr_map = {}

def swap_expressions(expression):
    """
    This function looks more complicated than it is: it simply
    replaces expressions such as agep >= 18 with a single variable, 
    so that sympy doesn't get confused and try to reduce
    the expression agep >= 18. 

    input: a string expression such as: (agep >= 18 & sex = 1 | sex = 2)

    As output, you get the replaced expression (A & B | C) and the expression map.
    ie: {"A":"agep >= 18", "B":"sex=1", "C":"sex=2"}
    """
    depth = 0 
    curr_expr = ""
    curr_letter = 0
    new_expr = ""
    for ch in expression:
        if ch == '(':
            depth += 1
            new_expr += ch
        elif ch == ')':
            depth -= 1
            if curr_expr:
                new_expr += letters[curr_letter]
                expr_map[letters[curr_letter]] = curr_expr
                curr_letter += 1
            new_expr += ch 
            curr_expr = ""
        elif ch == '|' or ch == '&':
            curr_expr = curr_expr.strip()
            if curr_expr:
                new_expr += letters[curr_letter]
                expr_map[letters[curr_letter]] = curr_expr
                curr_letter += 1
            new_expr += ch
            curr_expr = ""
        else:
            curr_expr += ch
    return new_expr, expr_map
            


with open('pglog.csv', 'r') as expressions_in:
    data_out = []
    """ read in a log of csv queries, where the 
    parsed predicate is stored in column 13 """
    expressions = csv.reader(expressions_in)
    for expr in expressions:
        #13 is our output
        debug = expr[13]
        if debug and "QUERY" in debug:
            # i store it in a weird form: QUERY -- <query> :: EXPRESSION -- <expr>, for parsing
            query, expression = debug.split("::")
            query = query.split("--")[1].strip()
            expression = expression.split("--")[1].strip()
            #format the expression so we can use it in sympy without confusing sympy
            expr, expr_map = swap_expressions(expression)
            #now send this expression to sympy
            if expr:
                print(expression)
                print(expr)
                output = []
                cnf_expr = to_cnf(expr)
                simplify_cnf = simplify_logic(expr)
                #now step through and replace the variables with the old expressions
                for arg in simplify_cnf.args:
                    if isinstance(arg, And):
                        #could be possible if simplify_logic produces a dnf, haven't seen it yet
                        pass
                    elif isinstance(arg, Or):
                        or_arg = arg
                        sublist = []
                        for oarg in or_arg.args:
                            var = expr_map[str(oarg)]
                            split_expr = var.split(" ")
                            sublist.append(split_expr)
                        output.append(sublist)
                    else:
                        var = expr_map[str(arg)].strip()
                        #avoid adding dummy variables induced by WHERE true in original query
                        if var[0] == 'p':
                            continue
                        split_expr = var.split(" ")
                        output.append([split_expr])

                data_out.append({"query":query, "expression":expression, "output":output})
                print(simplify_cnf)
                print(output)

    json.dump(data_out, open('expressions_normal_form.json', 'w'))

