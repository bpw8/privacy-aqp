#include "postgres.h"

#include "libpq/auth.h"
#include "parser/analyze.h"
#include "utils/ruleutils.h"
#include "utils/guc.h"
#include "utils/timestamp.h"
#include "lib/stringinfo.h"
#include "catalog/pg_operator.h"
#include "access/htup.h"
#include "access/htup_details.h"
#include "utils/syscache.h"
#include "utils/lsyscache.h"
#include "storage/fd.h"
#include "optimizer/tlist.h"

PG_MODULE_MAGIC;

void		_PG_init(void);


static post_parse_analyze_hook_type original_post_parse_analyze_hook  = NULL;
static int PG_QUERY_NUM = 1;

static char * get_expression_symbol(Oid opno) {
	HeapTuple	opertup;
	Form_pg_operator operform;
    char *tmp;
    opertup = SearchSysCache1(OPEROID, ObjectIdGetDatum(opno));
	if (!HeapTupleIsValid(opertup))
		        elog(ERROR, "cache lookup failed for operator %u", opno);
	operform = (Form_pg_operator) GETSTRUCT(opertup);
	tmp = NameStr(operform->oprname);
    ereport(WARNING, (errcode(ERRCODE_WARNING), errmsg("PRIVACY: expression is %s", tmp)));
    return tmp;
}

static char *get_DataType(Query *query, Var *var) {
    /* TODO: make this work on the actual data types. */ 
    return "INTEGER";
}

static char *get_colname(Query *query, int relation_num, int var_index) {
     ListCell *curr;
     RangeTblEntry *rte;
     List *col_names;

     rte = (RangeTblEntry *) list_nth(query->rtable, relation_num-1);
     col_names = ((Alias *) rte->eref)->colnames;
     curr = list_nth(col_names, var_index-1);
     return strVal(curr);
}

static char *get_colname_var(Query *query, Var *var) {
    return get_colname(query, var->varno, var->varattno);
}

static char *get_relname(Query *query, int driver_relation) {
    RangeTblEntry *rte;
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("get_relname, rtable size %d", list_length(query->rtable))));

    rte = (RangeTblEntry *) list_nth(query->rtable, driver_relation-1);
    return get_rel_name(rte->relid);
}

static char *get_alias_rte(RangeTblEntry *rte) {
    return ((Alias *) rte->eref)->aliasname;
}

static char *get_alias(Query *query, int relation_num) {
    RangeTblEntry *rte;
    rte = (RangeTblEntry *) list_nth(query->rtable, relation_num-1);
    return get_alias_rte(rte);
}

static char *get_Var_Str(ParseState *pstate, Query *query, Var *var) {
    Oid relation_num;
    Oid var_index;
    char *column;
    Value *value;

    StringInfoData buf;
	initStringInfo(&buf);

    relation_num = var->varno;
    var_index = var->varattno;
    /* Look up the column name by using the relation_num and var_index in the Query object. */ 
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("column info relation %d, var_index %d", relation_num, var_index)));
    column = get_colname(query, relation_num, var_index);
    
    appendStringInfo(&buf, "%s", column);

    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("buf.data %s", buf.data)));
    return buf.data;
}

static char *get_Const_Str(Node *cValue) {
    Datum datum;
    char *s;
    Const *con = (Const *) cValue;
    datum = con->constvalue;
    s = (char *) (&datum);
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("PRIVACY: clause const value %d", (int) s[0])));
    return s;
               
}


static char *get_OpExpr_Str(ParseState *pstate, Query *query, Node *node) {
    OpExpr *op;
    ListCell *arg;
    StringInfoData buf;
    Var *v;
    Node * cValue;
    RelabelType *t;
    char * symbol;
    char *column;
    int argNum = 0;
    initStringInfo(&buf);

    if (nodeTag(node) == T_OpExpr) {
        //T_OpExpr has two args, and one expression
        /* This is the base expression argument. */ 
        op = (OpExpr *)node;
        symbol = get_expression_symbol(op->opno); 
        /* For every arg, get the column name and create a grounded/ungrounded name for the bool expr. */ 
        foreach(arg, op->args) {
            
            cValue = lfirst(arg);

            ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("PRIVACY: cValue type is %d", nodeTag(cValue))));
            /* If it's a T_Relabel_Type, then the actual Var is hidden in cValue->arg, and might be of type Var or Const. */
            if (nodeTag(cValue) == T_RelabelType) {
                t = (RelabelType *) cValue;
                ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("PRIVACY: Relabel type arg is %d", nodeTag(t->arg))));
                if (nodeTag(t->arg) == T_Var) {
                    v = (Var *)t->arg;
                    column = get_Var_Str(pstate, query, v);
                    appendStringInfo(&buf, "%s", column);
                }
            } else if (nodeTag(cValue) == T_Var) {
                column = get_Var_Str(pstate, query, (Var *) cValue);
                appendStringInfo(&buf, "%s", column);
            } else if (nodeTag(cValue) == T_Const) {
                column = get_Const_Str(cValue);
                appendStringInfo(&buf, "%d", (int) column[0]);
            }

            /* This is just to keep track of when to insert the comparator symbol. */ 
            if (argNum == 0) {
                appendStringInfo(&buf, " %s ", symbol);
            }
            argNum++;
        }

    }
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("PRIVACY: opExprStr %s", buf.data)));
    return buf.data;

}

static char * get_BoolExpr_Str(ParseState *pstate, Query *query, Node *node) {
    StringInfoData buf;
    BoolExpr * boolop;
    Node *expr;
    ListCell *arg;
    int argNum = 0;
    char *exprStr;

    initStringInfo(&buf);
 
    boolop = (BoolExpr *)node;
    
    foreach(arg, boolop->args) {
       expr = lfirst(arg);
       if (nodeTag(expr) == T_BoolExpr) {
            //ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("PRIVACY: Found a boolean expression")));
            exprStr = get_BoolExpr_Str(pstate, query, expr);
       } else if (nodeTag(expr) == T_OpExpr) {
            //ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("PRIVACY: Found a basic op expression")));
            exprStr = get_OpExpr_Str(pstate, query, expr);
       } else {

            //ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("PRIVACY: Found somethign else")));
       }
       if (argNum > 0) {
            /* For now we assume conjunctive queries, but nothing is stopping a mixture. */ 
            if (boolop->boolop == AND_EXPR) {
                appendStringInfo(&buf, " & ");
            } else if (boolop->boolop == OR_EXPR) {
                appendStringInfo(&buf, " | ");
            }
       }
       appendStringInfo(&buf, "(%s)", exprStr); 
       argNum++;
    }
    return buf.data;

}


static void privacy_aqp_analyze_parse_tree(ParseState *pstate, Query *query)
{

    /* First, we operate a simple linear query. Extract the WHERE clause and 
    transform to CNF. */

    StringInfoData buf;
    Node *node;
    char *boolExpr;
    char *nodeOut;

    TargetEntry *entry;
    ListCell *target;
    ListCell *lc;
    ListCell *colCell;

    int length = 0;
    int sublength = 0;

    FILE *file;
    FILE *file2;

    file2 = fopen("output.lock", "w");
    fprintf(file2, "regular string");
    int fd2 = fileno(file2);
    fsync(fd2);
    fclose(file2);
    

    initStringInfo(&buf);
    

    appendStringInfo(&buf, "{\"query_id\": %d, \"query\": \"%s\", \"topTargetList\":[", PG_QUERY_NUM, pstate->p_sourcetext);
    length = 1;
    foreach(target, query->targetList) {
        TargetEntry *entry = lfirst_node(TargetEntry, target);
        if (nodeTag(entry->expr) == T_Var) {
            appendStringInfo(&buf, "{\"name\":\"%s\", \"type\": \"var\"}", entry->resname);
        }
        if (nodeTag(entry->expr) == T_Aggref) {
            Aggref *expr = (Aggref *) node;  
            appendStringInfo(&buf, "{\"name\":\"%s\", \"type\": \"agg\"}", entry->resname);
        }
        if (length < query->targetList->length) {
            appendStringInfo(&buf, ",");
        }
        length++;
    }
    appendStringInfo(&buf, "],");

    appendStringInfo(&buf, "\"joinTargetLists\":[");
    ereport(WARNING, (errcode(ERRCODE_WARNING), errmsg("TARGET -- %s", buf.data)));
    /* This parses the query join expression and produces a CNF formatted string. */ 
    if (query->jointree) {

        foreach(lc, query->jointree->fromlist) {
           //node = (Node *)lc;
          node = lfirst(lc);
          RangeTblRef *rtr = lfirst_node(RangeTblRef, lc);
          //RangeTblEntry *rte = rt_fetch(rtr->rtindex, parsetree->rtable);  
           nodeOut = nodeToString(node);
        
           ereport(WARNING, (errcode(ERRCODE_WARNING), errmsg("Node Output-- %s", nodeOut)));
        }

        length = 1;
        foreach(lc, query->rtable) {
            RangeTblEntry *entry = lfirst_node(RangeTblEntry, lc);
            Alias *eref = entry->eref;
            appendStringInfo(&buf, "{\"targetList\":[");

            sublength = 1;
            foreach(colCell, eref->colnames) {
                appendStringInfo(&buf, "{\"name\":\"%s\", \"type\": \"var\"}", strVal(lfirst(colCell)));
                if (sublength < eref->colnames->length) {
                    appendStringInfo(&buf, ",");
                }
                sublength++;
            }
            appendStringInfo(&buf, "]}");
            if (length < query->rtable->length) {
                appendStringInfo(&buf, ",");
            }
            length++;
        }

        /* Get the join tree qual node. */ 
        /*node = query->jointree->quals;
        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("Got the join tree.")));
        nodeOut = nodeToString(node);
        if (!strcmp(nodeOut, "<>")) {
            ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("join tree is empty %s", nodeOut)));
            return;
        }
        ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("PRIVACY: node string is %s", nodeOut)));
        if (nodeTag(node) == T_BoolExpr) {
            ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("PRIVACY: Found a boolean expression")));
            boolExpr = get_BoolExpr_Str(pstate, query, node);
        } else if (nodeTag(node) == T_OpExpr) {
            ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("PRIVACY: Found a basic op expression")));
            boolExpr = get_OpExpr_Str(pstate, query, node);
        } */

        appendStringInfo(&buf, "]}");
        ereport(WARNING, (errcode(ERRCODE_WARNING), errmsg("JSON data -- %s", buf.data)));
        
        file = fopen("output.json", "a");
        fprintf(file, "%s\n", buf.data);
        int fd = fileno(file);
        fflush(file);
        fsync(fd);
        fclose(file);
    }
    

    /* Make sure that we have finished writing in output.json */
 
    unlink("output.lock");

}
 

/*
 * Module Load Callback
 */
void
_PG_init(void)
{
	/* Install Hooks */
	original_post_parse_analyze_hook = post_parse_analyze_hook;
	post_parse_analyze_hook = privacy_aqp_analyze_parse_tree;
    ereport(DEBUG1, (errcode(ERRCODE_WARNING), errmsg("PAQP: PAQP module initialized, hook set.")));
}
