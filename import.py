import pandas as pd


from sqlalchemy import create_engine
engine = create_engine('postgresql://@localhost:5432/census')

df = pd.read_csv('sample_persons.csv')
df.columns = [c.lower() for c in df.columns] #postgres doesn't like capitals or spaces
df.to_sql("hh_persons", engine)

df = pd.read_csv('housing_sample.csv')
df.columns = [c.lower() for c in df.columns] #postgres doesn't like capitals or spaces
df.to_sql("housing", engine)

df = pd.read_csv('geo.csv')
df.columns = [c.lower() for c in df.columns] #postgres doesn't like capitals or spaces
df.to_sql("geo", engine)







